PinBallin
------------

A simple Pinball game generated in UE4

## Controls ##

Camera angle one:	1, Numpad 1, Lowest Gamepad Button
Camera angle two:	2, Numpad 2, Left Gamepad Button
Camera angle three:	3, Numpad 3, Top Gamepad Button
Camera angle four:	4, Numpad 4, Right Gamepad Button

Left Bumpers:		A, Left Control, Left Arrow, Left D-Pad, Left Shoulder
Right Bumpers:		D, Right Control, Right Arrow, Right D-Pad, Right Shoulder

Plunger:		S, Space Bar, Down Arrow, Down D-pad

Tilt Left:		Left Shift, Delete
Tilt Right:		Right shift, Page Down

Exit Key:               Escape

SUPER SECRET FORCE SPAWN BALL CHEAT KEY: P

## Credits ##

Made by Thomas Hawco
Pinball Assets released under creative commons license by Epic of Japan.
SuperGrid Assets released via unreal marketplace under Epic's marketplace license.
Epic Pinball Title Music composed by Robert Allen. Arrangement by Thomas Hawco


##Notes##

Ball can currently jump, this may be changed in a future version.

Extra ball spawn may occasionally spawn an additional ball. Yay 2 balls for the price of one!

Ramp into rail currently gives the most single points, thus hitting it after growing your multiplier is currently the fastest way to highscores.

Flap the at the top may get stuck open in some cases.